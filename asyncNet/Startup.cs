﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using Microsoft.Extensions.Logging.Debug;

namespace asyncNet
{
  public class Startup
  {
    public void ConfigureServices(IServiceCollection services)
    {
      services.AddLogging(builder =>
      {
        builder.AddConsole()
                  .AddDebug()
                  .AddFilter<ConsoleLoggerProvider>(category: null, level: LogLevel.Debug)
                  .AddFilter<DebugLoggerProvider>(category: null, level: LogLevel.Debug);
      });
    }
    public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }
      app.UseWebSockets();
      app.UseFileServer();

      app.Use(async (context, next) =>
      {
        if (context.Request.Path == "/ws")
        {
          if (context.WebSockets.IsWebSocketRequest)
          {
            WebSocket webSocket = await context.WebSockets.AcceptWebSocketAsync();
            await Echo(context, webSocket);

          }
          else
          {
            context.Response.StatusCode = 400;
          }
        }
        else
        {
          await next();
        }


      });
    }
    #region Echo
    private async Task Echo(HttpContext context, WebSocket webSocket)
    {
      var buffer = new byte[1024 * 4];
      WebSocketReceiveResult result = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
      while (!result.CloseStatus.HasValue)
      {
        var message = Encoding.ASCII.GetString(buffer, 0, result.Count);
        System.Console.WriteLine(Encoding.ASCII.GetString(buffer, 0, result.Count));
        var bytes = Encoding.ASCII.GetBytes(processMath(message));
        var arraySegment = new ArraySegment<byte>(bytes);
        await webSocket.SendAsync(arraySegment, WebSocketMessageType.Text, true, CancellationToken.None);

        result = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
        

      }
      await webSocket.CloseAsync(result.CloseStatus.Value, result.CloseStatusDescription, CancellationToken.None);
    }
    private String processMath(String message) {
      var first = Double.Parse(message.Split("$")[0]);
      var second = Double.Parse(message.Split("$")[2]);
      var operation = message.Split("$")[1];
      switch (operation)
      {
        case "S":
          return (first + second).ToString();
        case "R":
          return (first - second).ToString();
        case "M":
          return (first * second).ToString();
        case "D":
          return (first / second).ToString();
        default:
          return "WRONG FORMAT";
      }
    }
    #endregion
  }

}
